# Development Container: micro ROS

This repository is a scaffolding encompassing the workspace area along with the devContainer containing all the dependencies required  for developing IoT applications. A template workspace named "ws1" is placed inside workspaces folder for reference.

## Notes

After cloning the repo, if you are working in submodules, make sure to run the below command as a one-time setup, to automatically update your parent repo also to the commit of your submodules.
`git submodule foreach --recursive git config core.hooksPath .githooks/`

## License

Dummy License
